//
//  GCAppDelegate.h
//  GCNetwork
//
//  Created by chanjh on 06/15/2018.
//  Copyright (c) 2018 chanjh. All rights reserved.
//

@import UIKit;

@interface GCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
