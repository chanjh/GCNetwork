//
//  GetExampleAPI.h
//  GCNetworkDemo
//
//  Created by 陈嘉豪 on 2018/6/13.
//  Copyright © 2018年 Gill Chan. All rights reserved.
//

#import "GCBaseRequest.h"

@interface GetExampleAPI : GCBaseRequest <GCAPIRequest>

@end
