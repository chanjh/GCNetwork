//
//  GCViewController.m
//  GCNetwork
//
//  Created by chanjh on 06/15/2018.
//  Copyright (c) 2018 chanjh. All rights reserved.
//

#import "GCViewController.h"
#import "GetExampleAPI.h"
#import <GCNetwork/GCRequestGroup.h>
#import "RhymeAPI.h"

@interface GCViewController ()<GCRequestGroupDelegate>

@end

@implementation GCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    RhymeAPI *api = [[RhymeAPI alloc]initWithSYSearchKeyword:@"先"];
    [api startWithBlockSuccess:^(__kindof GCBaseRequest *request) {
        
    } failure:^(__kindof GCBaseRequest *request, NSError *error) {
        
    }];
}

- (void)startGroup{
    NSMutableArray *a = [NSMutableArray array];
    for (NSInteger i = 0; i<5; i++){
        GetExampleAPI *api = [[GetExampleAPI alloc]init];
        [a addObject:api];
    }
    GCRequestGroup *group = [[GCRequestGroup alloc]initWithGropMode:GCRequestGroupModeChian];
    group.delegate = self;
    [group addRequests:[a copy]];
    [group start];
}

- (void)didFinishedAllRequestsOnRequestGroup:(GCRequestGroup *)group{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
