//
//  GCAppDelegate.m
//  GCNetwork
//
//  Created by chanjh on 06/15/2018.
//  Copyright (c) 2018 chanjh. All rights reserved.
//

#import "GCAppDelegate.h"
#import <GCNetwork/GCNetwork.h>

@implementation GCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    GCNetworkConfig *gcConfig = [GCNetworkConfig sharedInstance];
    gcConfig.mainBaseUrl = @"http://sdf.sou-yun.com/api";
    return YES;
}

@end
