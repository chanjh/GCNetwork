//
//  main.m
//  GCNetwork
//
//  Created by chanjh on 06/15/2018.
//  Copyright (c) 2018 chanjh. All rights reserved.
//

@import UIKit;
#import "GCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GCAppDelegate class]));
    }
}
