//
//  RhymeAPI.m
//  搜韵
//
//  Created by 陈嘉豪 on 2018/7/14.
//  Copyright © 2018年 Gill Chan. All rights reserved.
//

#import "RhymeAPI.h"

@implementation RhymeAPI{
    NSString *_key;
}
- (instancetype)initWithSYSearchKeyword:(NSString *)keyword{
    if(self = [super init]){
        _key = keyword;
    }
    return self;
}

- (GCResponseSerializerType)responseSerializerType{
    return GCResponseSerializerTypeHTTP;
}

- (NSDictionary *)requestArgument{
    if(_key.length){
        return @{@"id":_key};
    }
    return nil;
}

- (GCRequestMethod)requestMethod{
    return GCRequestMethodGet;
}

- (NSString *)requestUrl{
    return @"/v1/Rhyme";
}

@end
