//
//  RhymeAPI.h
//  搜韵
//
//  Created by 陈嘉豪 on 2018/7/14.
//  Copyright © 2018年 Gill Chan. All rights reserved.
//

#import "GCBaseRequest.h"

@interface RhymeAPI : GCBaseRequest <GCAPIRequest>
- (instancetype)initWithSYSearchKeyword:(NSString *)keyword;
@end
