#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "JHDownloadConfig.h"
#import "JHDownloader.h"
#import "JHDownloadManager.h"
#import "GCBaseRequest.h"
#import "GCNetwork.h"
#import "GCNetworkAgent.h"
#import "GCNetworkConfig.h"
#import "GCNetworkConstant.h"
#import "GCReformerProtocol.h"
#import "Reachability.h"

FOUNDATION_EXPORT double GCNetworkVersionNumber;
FOUNDATION_EXPORT const unsigned char GCNetworkVersionString[];

