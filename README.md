# GCNetwork

[![CI Status](https://img.shields.io/travis/chanjh/GCNetwork.svg?style=flat)](https://travis-ci.org/chanjh/GCNetwork)
[![Version](https://img.shields.io/cocoapods/v/GCNetwork.svg?style=flat)](https://cocoapods.org/pods/GCNetwork)
[![License](https://img.shields.io/cocoapods/l/GCNetwork.svg?style=flat)](https://cocoapods.org/pods/GCNetwork)
[![Platform](https://img.shields.io/cocoapods/p/GCNetwork.svg?style=flat)](https://cocoapods.org/pods/GCNetwork)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GCNetwork is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GCNetwork'
```

## Author

chanjh, jiahao0408@gmail.com

## License

GCNetwork is available under the MIT license. See the LICENSE file for more info.
