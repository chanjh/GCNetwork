Pod::Spec.new do |s|
  s.name             = "GCNetwork"    #名称
  s.version          = "0.2.1"          #版本号
  s.summary          = "一个简易网络层框架"     #简短介绍，下面是详细介绍
  s.description      = <<-DESC
                       Testing Private Podspec.

                       * Markdown format.
                       * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "https://gitlab.com/chanjh/GCNetwork"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'              #开源协议
  s.author           = { "Gill Chan" => "hi@chanjh.com" }                   #作者信息
  s.source           = { :git => "https://gitlab.com/chanjh/GCNetwork.git"}
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'                       #多媒体介绍地址

  s.platform     = :ios, '7.0'            #支持的平台及版本
  s.requires_arc = true                   #是否使用ARC，如果指定具体文件，则具体的问题使用ARC

  s.source_files = 'GCNetwork/Classes/**/*'     #代码源文件地址，**/*表示Classes目录及其子目录下所有文件，如果有多个目录下则用逗号分开，如果需要在项目中分组显示，这里也要做相应的设置
  # s.resource_bundles = {
  #   'PodTestLibrary' => ['GCNetwork/Assets/*.png']
  # }                                       #资源文件地址

  s.public_header_files = 'GCNetwork/Classes/**/*.h'
  # s.frameworks = 'UIKit'                  #所需的framework，多个用逗号隔开
  s.dependency 'AFNetworking', '3.2.1'
  s.dependency 'YYCache', '1.0.4'

end
