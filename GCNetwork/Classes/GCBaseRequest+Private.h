//
//  GCBaseRequest+Private.h
//  GCNetwork
//
//  Created by 陈嘉豪 on 2018/6/27.
//

#import <Foundation/Foundation.h>
#import "GCBaseRequest.h"

@interface GCBaseRequest(Private)

@property (nonatomic, assign) GCRequestStatus status;
- (void)changeStatus:(GCRequestStatus)status;

@end
