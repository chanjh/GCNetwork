//
//  GCRequestGroup.h
//  AFNetworking
//
//  Created by 陈嘉豪 on 2018/7/13.
//

#import <Foundation/Foundation.h>
#import "GCBaseRequest.h"

typedef enum : NSUInteger {
    GCRequestGroupModeNoDefine = 0, //
    GCRequestGroupModeDisorder,     // 无序
    GCRequestGroupModeChian,        // 链式
} GCRequestGroupMode;

@class GCRequestGroup;
@protocol GCRequestGroupDelegate <NSObject>
@optional
- (void)didFinishedAllRequestsOnRequestGroup:(GCRequestGroup *)group;
@end

@interface GCRequestGroup : NSObject
@property (nonatomic, copy, readonly) NSArray <GCBaseRequest *> *requests;
@property (nonatomic, copy, readonly) NSArray <GCBaseRequest *> *successRequests;
@property (nonatomic, copy, readonly) NSArray <GCBaseRequest *> *failureRequests;
@property (nonatomic, copy, readonly) NSArray <GCBaseRequest *> *finishRequests;

@property (nonatomic, weak) id<GCRequestGroupDelegate> delegate;
- (instancetype)initWithGropMode:(GCRequestGroupMode)mode;
- (void)addRequests:(NSArray <GCBaseRequest *> *)requests;
- (void)start;
- (void)stop;
@end
