//
//  GCBaseRequest.m
//
//  Created by 陈嘉豪 on 2017/9/10.
//  Copyright © 2017年 Gill Chan. All rights reserved.
//

#import "GCBaseRequest.h"
#import "GCNetworkConfig.h"
#import "GCNetworkAgent.h"
#import "YYCache.h"

@interface GCBaseRequest ()

@property (nonatomic, strong) id cacheJson;
@property (nonatomic, weak) id<GCAPIRequest> child;
@property (nonatomic, strong) GCNetworkConfig *config;
@property (nonatomic, strong) NSMutableArray *requestAccessories;
@property (nonatomic, strong) GCNetworkAgent *shareAgent;

@end

#pragma mark -

@implementation GCBaseRequest

#pragma mark - life cycle

- (instancetype)init
{
    if(self = [super init])
    {
        if([self conformsToProtocol:@protocol(GCAPIRequest)])
        {
            _child = (id<GCAPIRequest>)self;
        }
        else
        {
            // MARK: 可以在创建的类没有遵循协议时抛出异常
            NSException *exception = [[NSException alloc]initWithName:@"GCBaseRequest Exception"
                                                               reason:@"API 中没有遵循 GCAPIRequest 协议" userInfo:nil];
            @throw exception;
        }
        _config = [GCNetworkConfig sharedInstance];
        _shareAgent = [[GCNetworkAgent alloc]init];
        _status = GCRequestStatusInit;
    }
    return self;
}

#pragma mark - network request

/**
 block 回调方式
 
 @param success 成功回调
 @param failure 失败回调
 */
- (void)startWithBlockSuccess:(GCRequestCompletionBlock)success
                      failure:(GCRequestFailureBlock)failure{
    self.successCompletionBlock = success;
    self.failureCompletionBlock = failure;
    [self start];
}

/**
 block 回调方式
 
 @param progress 进度回调
 @param success 成功回调
 @param failure 失败回调
 */
- (void)startWithBlockProgress:(void (^)(NSProgress *))progress
                       success:(GCRequestCompletionBlock)success
                       failure:(GCRequestFailureBlock)failure{
    self.progressBlock = progress;
    self.successCompletionBlock = success;
    self.failureCompletionBlock = failure;
    [self start];
}

/**
 block 回调方式
 
 @param success 成功回调
 @param failure 失败回调
 @param finished 请求完成后的回调
 */
- (void)startWithBlockSuccess:(GCRequestCompletionBlock)success
                      failure:(GCRequestFailureBlock)failure
                     finished:(GCRequestFinishedBlock)finished{
    self.successCompletionBlock = success;
    self.failureCompletionBlock = failure;
    self.finishedCompletionBlock = finished;
    [self start];
}

/**
 block 回调方式
 
 @param progress 进度回调
 @param success 成功回调
 @param failure 失败回调
 @param finished 请求完成后的回调
 */
- (void)startWithBlockProgress:(void (^)(NSProgress *))progress
                       success:(GCRequestCompletionBlock)success
                       failure:(GCRequestFailureBlock)failure
                      finished:(GCRequestFinishedBlock)finished{
    self.progressBlock = progress;
    self.successCompletionBlock = success;
    self.failureCompletionBlock = failure;
    self.finishedCompletionBlock = finished;
    [self start];
}

/**
 开始一个请求
 */
- (void)start{
    [self.shareAgent addRequest:self];
}

/**
 停止一个请求
 */
- (void)stop{
    self.delegate = nil;
    [self.shareAgent cancelRequest:self];
}

/**
 清理回调块
 */
- (void)clearCompletionBlock {
    self.successCompletionBlock = nil;
    self.failureCompletionBlock = nil;
    self.finishedCompletionBlock = nil;
    self.progressBlock = nil;
}

#pragma mark - handle response object

/**
 对 _rawJSONResponseObject 进行过滤,返回过滤后的 json 数据
 
 @return 过滤后的 json 数据，如果没有过滤则返回 nil
 */
- (id)filteredJSONResponseObject {
    id filteredObject = nil;
    // 是否统一过滤，通过 GCNetworkConfig 统一加工 response，
    BOOL ignoreUniformFiltering = ([self.child respondsToSelector:@selector(ignoreResponseObjectUniformFiltering)] && [self.child ignoreResponseObjectUniformFiltering]) || [self.child respondsToSelector:@selector(ignoreResponseObjectUniformFiltering)];
    if (!ignoreUniformFiltering && self.config.uniReformer && [self.config.uniReformer respondsToSelector:@selector(uniformlyFilterResponseObject:)]) {
        // 全部 API 过滤，先过滤掉不要的信息
        filteredObject = [self.config.uniReformer uniformlyFilterResponseObject:_rawJSONResponseObject];
    }else{
        filteredObject = _rawJSONResponseObject;
    }
    
    // 是否配置独立过滤
    if ([self.child respondsToSelector:@selector(reformer)]){
        id<GCReformerProtocol> reformer = [self.child reformer];
        //子API各自过滤
        if ([reformer respondsToSelector:@selector(specificallyFilterResponseObject:)]) {
            filteredObject = [reformer specificallyFilterResponseObject:filteredObject];
        }
    }
    return filteredObject;
}

/**
 对 _rawJSONResponseObject 进行过滤、建模，返回建模后的 model 数组
 
 @return 建模后的 model 数组
 */
- (NSArray *)modeledResponseObject {
    // 是否过滤
    if (self.filteredJSONResponseObject) {
        if ([self.child respondsToSelector:@selector(modelingFormJSONResponseObject:)]) {
            return  [self.child modelingFormJSONResponseObject:self.filteredJSONResponseObject];
        }
    }
    
    // 是否建模
    if ([self.child respondsToSelector:@selector(modelingFormJSONResponseObject:)]){
        return [self.child modelingFormJSONResponseObject:_rawJSONResponseObject];
    }
    
    
    // 无需建模，直接返回空数组
    return @[];
    
}
/**
 缓存地址数据
 
 @return 缓存的数据
 */
- (id)cacheJson{
    if (_cacheJson) {
        return _cacheJson;
    }
    else{
        YYCache *yyCache=[YYCache cacheWithName:@"cacheJson"];
        return [yyCache.diskCache objectForKey:[self urlStringForQuery]];
    }
}

#pragma mark - URL config

/**
 组装 URL 地址
 
 @return API 所需的 URL 地址
 */
- (NSString *)urlString{
    NSString *baseUrl = nil;
    // TODO: 使用副地址
    if ([self.child respondsToSelector:@selector(useViceUrl)] && [self.child useViceUrl]){
        baseUrl = self.config.viceBaseUrl;
    }
    // TODO: 使用主地址
    else{
        baseUrl = self.config.mainBaseUrl;
    }
    if (baseUrl) {
        // TODO: 使用自定义地址
        if ( [self.child respondsToSelector:@selector(useCustomApiMethodName)] && [self.child useCustomApiMethodName]) {
            return [self.child requestUrl];
        }
        NSString *urlString = baseUrl;
        
        if ([self.child respondsToSelector:@selector(requestUrl)]) {
            urlString = [baseUrl stringByAppendingString:[self.child requestUrl]];
        }
        
        // TODO: 当 POST 下参数不在 body 中时，拼接 queryArgument 中的地址
        if (self.queryArgument && [self.queryArgument isKindOfClass:[NSDictionary class]]) {
            return [urlString stringByAppendingString:[self urlStringForQuery]];
        }
        return urlString;
    }
    return [self.child requestUrl];
}

/**
 初始化 Query 的 URL
 
 @return Query 的 URl
 */
- (NSString *)urlStringForQuery{
    NSMutableString *urlString = [[NSMutableString alloc] init];
    [urlString appendString:@"?"];
    NSArray *keyArray = [self.queryArgument allKeys];
    for(NSString *key in keyArray)
    {
        [urlString appendFormat:@"%@=%@&", key, self.queryArgument[key]];
    }
    [urlString deleteCharactersInRange:NSMakeRange(urlString.length - 1, 1)];
    return [urlString copy];
}

@end

