//
//  GCBaseReformer.h
//  GCNetwork
//
//  Created by 陈嘉豪 on 2018/4/2.
//  Copyright © 2018年 Gill Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 用于处理 response 的协议
 */
@protocol GCReformerProtocol <NSObject>
@optional
# pragma mark - 设置统一的过滤
/**
 *  用于统一加工参数，返回处理后的参数值
 *
 *  @param argument 参数
 *  @param queryArgument query 信息，某些POST请求希望参数不放在body里面，需要用Query来附带数据，这时候就用到queryArgument属性。
 *
 *  @return 处理后的参数
 */
- (NSDictionary *)processArgumentWithRequest:(NSDictionary *)argument query:(NSDictionary *)queryArgument;

/**
 *  用于全部API统一过滤response，返回处理后response
 *
 *  @param responseObject response
 *
 *  @return 处理后的response
 */
- (id)uniformlyFilterResponseObject: (id)responseObject;

# pragma mark - 单个 Reformer 的过滤设计
/**
 API各自过滤response,返回处理后response
 
 @param responseObject responseObject
 @return 处理后response
 */
- (id)specificallyFilterResponseObject: (id)responseObject;

/**
 用于判断返回数据是否合法
 
 @param responseObject 请求获取的数据
 @return 数据合法与否
 */
- (BOOL)validResponseObject:(id)responseObject;

@end


