//
//  GCRequestGroup.m
//  AFNetworking
//
//  Created by 陈嘉豪 on 2018/7/13.
//

#import "GCRequestGroup.h"

@interface GCRequestGroup()
@property (nonatomic, assign) GCRequestGroupMode mode;
@property (nonatomic, strong) NSMutableArray <GCBaseRequest *> *mutableRequests;
@property (nonatomic, strong) NSMutableArray <GCBaseRequest *> *successArray;
@property (nonatomic, strong) NSMutableArray <GCBaseRequest *> *failureArray;
@property (nonatomic, strong) NSMutableArray <GCBaseRequest *> *finishArray;
@property (nonatomic, strong) NSOperationQueue *queue;
@end

@implementation GCRequestGroup
- (instancetype)initWithGropMode:(GCRequestGroupMode)mode{
    if(self = [super init]){
        self.mode = mode;
    }
    return self;
}
- (void)addRequests:(NSArray<GCBaseRequest *> *)requests{
    if(requests.count){
        [self.mutableRequests addObjectsFromArray:requests];
    }
}

- (void)start{
    if(self.mutableRequests.count){
        if(self.mode == GCRequestGroupModeDisorder){
            for (GCBaseRequest *request in self.mutableRequests){
                [self startForRequest:request withCompletion:nil];
            }
        }else if(self.mode == GCRequestGroupModeChian){
            dispatch_semaphore_t sema = dispatch_semaphore_create(5);
            for (GCBaseRequest *request in self.mutableRequests){
                NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
                    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                    [self startForRequest:request withCompletion:^{
                        dispatch_semaphore_signal(sema);
                    }];
                }];
                [self.queue addOperation:operation];
            }
        }
    }
}

- (void)stop{
    if(!self.mutableRequests.count){
        return;
    }
    if(self.mode == GCRequestGroupModeDisorder){
        for (GCBaseRequest *request in self.mutableRequests){
            [request stop];
        }
    }else if(self.mode == GCRequestGroupModeChian && self.queue.operationCount){
        [self.queue cancelAllOperations];
        for (GCBaseRequest *request in self.mutableRequests){
            [request stop];
        }
    }
}

- (void)startForRequest:(GCBaseRequest *)request withCompletion:(void(^)(void))completion{
    [request startWithBlockSuccess:^(__kindof GCBaseRequest *request) {
        @synchronized (self) {
            [self.successArray addObject:request];
        }
    } failure:^(__kindof GCBaseRequest *request, NSError *error) {
        @synchronized (self) {
            [self.failureArray addObject:request];
        }
    } finished:^(__kindof GCBaseRequest *request, NSError *error) {
        @synchronized (self) {
            [self.finishArray addObject:request];
        }
        if(completion){
            completion();
        }
        if(self.finishArray.count == self.mutableRequests.count){
            // 所有请求完毕
            if([self.delegate respondsToSelector:@selector(didFinishedAllRequestsOnRequestGroup:)]){
                [self.delegate didFinishedAllRequestsOnRequestGroup:self];
            }
        }
    }];
}


# pragma mark -
- (NSOperationQueue *)queue{
    if(!_queue){
        _queue = [[NSOperationQueue alloc] init];
    }
    return _queue;
}
- (NSArray<GCBaseRequest *> *)successRequests{
    return [self.successArray copy];
}
- (NSArray<GCBaseRequest *> *)failureRequests{
    return [self.failureArray copy];
}
- (NSArray<GCBaseRequest *> *)finishRequests{
    return [self.finishArray copy];
}
- (NSArray<GCBaseRequest *> *)requests{
    return [self.mutableRequests copy];
}
- (NSMutableArray <GCBaseRequest *> *)mutableRequests{
    if(!_mutableRequests){
        _mutableRequests = [NSMutableArray array];
    }
    return _mutableRequests;
}
- (NSMutableArray<GCBaseRequest *> *)successArray{
    if(!_successArray){
        _successArray = [NSMutableArray array];
    }
    return _successArray;
}
- (NSMutableArray<GCBaseRequest *> *)failureArray{
    if(!_failureArray){
        _failureArray = [NSMutableArray array];
    }
    return _failureArray;
}
- (NSMutableArray<GCBaseRequest *> *)finishArray{
    if(!_finishArray){
        _finishArray = [NSMutableArray array];
    }
    return _finishArray;
}
@end
