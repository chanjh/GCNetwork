//
//  GCNetworkAgent.h
//
//  Created by 陈嘉豪 on 2017/9/10.
//  Copyright © 2017年 Gill Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GCBaseRequest;

#define k_GCNetworkAgentNotiNameForConnecting   @"k_GCNetworkAgentNotiNameForConnecting"
#define k_GCNetworkAgentNotiNameForUnconnecting @"k_GCNetworkAgentNotiNameForUnconnecting"

@interface GCNetworkAgent : NSObject

//+ (instancetype)shareInstance;

- (void)addRequest:(GCBaseRequest *)request;
- (void)cancelRequest:(GCBaseRequest *)request;

// SharedAgent 是否有任务正在联网中
//+ (BOOL)isAgentBusy;

- (BOOL)isAgentBusy;

@end
