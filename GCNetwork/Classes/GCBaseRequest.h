//
//  GCBaseRequest.h
//
//  Created by 陈嘉豪 on 2017/9/10.
//  Copyright © 2017年 Gill Chan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "GCReformerProtocol.h"
#import "GCNetworkConstant.h"

#pragma mark - type define

@class GCBaseRequest;
typedef void (^AFConstructingBlock)(id<AFMultipartFormData> formData);
typedef void (^GCRequestCompletionBlock)(__kindof GCBaseRequest *request);
typedef void (^GCRequestFailureBlock)(__kindof GCBaseRequest *request, NSError *error);
typedef void (^GCRequestFinishedBlock)(__kindof GCBaseRequest *request, NSError *error);

typedef NS_ENUM(NSInteger , GCRequestMethod) {
    GCRequestMethodGet = 0,
    GCRequestMethodPost,
    GCRequestMethodHead
};

typedef NS_ENUM(NSInteger , GCRequestSerializerType) {
    GCRequestSerializerTypeHTTP = 0,
    GCRequestSerializerTypeJSON,
};

typedef NS_ENUM(NSInteger , GCResponseSerializerType) {
    GCResponseSerializerTypeHTTP = 0,
    GCResponseSerializerTypeJSON,
};

typedef NS_ENUM(NSInteger , GCRequestStatus) {
    GCRequestStatusInit = 0,    // 完成初始化
    GCRequestStatusConnecting,  // 开始请求
    GCRequestStatusSuccess,     // 请求成功
    GCRequestStatusFailure,     // 请求失败
};

#pragma mark - class protocol

/*--------------------------------------------*/
/**
 所有继承自 GCBaseRequest 的 API 都必须遵守本协议中的 requestUrl 与 requestMethod 方法。
 */
@protocol GCAPIRequest <NSObject>

@required

/*--------------------------------------------*/
// MARK: base config


/**
 *  请求方式，包括 Get、Post、Head、Put、Delete、Patch，具体查看 GCRequestMethod
 *
 *  @return 请求方式
 */
- (GCRequestMethod)requestMethod;

/**
 api 请求地址
 当主地址不为空时，应返回资源的路径
 当主地址为空时，可返回整个 URL
 
 @return api 请求地址
 */
- (NSString *)requestUrl;

@optional
/*--------------------------------------------*/
// MARK: Reformer
- (id<GCReformerProtocol>)reformer;

/*--------------------------------------------*/
// MARK: URL config
/**
 *  可以使用两个根地址，比如可能会用到测试地址、https 之类的
 *
 *  @return 是否使用副 URL
 */
- (BOOL)useViceUrl;

/**
 *  是否使用自定义的接口地址，也就是不会使用 mainBaseUrl 或 viceBaseUrl，这时候在 requestUrl 就可以是用自定义的接口地址了
 *
 *  @return 是否使用自定义的接口地址
 */
- (BOOL)useCustomApiMethodName;

/*--------------------------------------------*/
//MARK: resonseObject config

/**
 对网络请求返回的 JSON 数据进行建模处理，返回 model 数组
 
 @param JSONResponseObject 网络请求返回的JSON数据
 @return 建模后的model数组
 */
- (NSArray *)modelingFormJSONResponseObject:(id)JSONResponseObject;

/**
 *  是否忽略统一的参数加工，默认返回否，resonseObject 将返回加工后的数据。
 *
 *  @return 返回 YES，那么 rawJSONResponseObject 将返回原始的数据
 */
- (BOOL)ignoreResponseObjectUniformFiltering;

/**
 检查请求返回的数据是否不可用，默认返回否，具体判断方法可以通过API自行设定
 
 @param responseObject 请求返回的数据
 @return 数据是否合法
 */
- (BOOL)isInvalidResponseObject:(id)responseObject;

/*--------------------------------------------*/
//MARK: cache config
/**
 *  是否缓存数据 response 数据
 *
 *  @return 是否缓存数据 response 数据
 */
- (BOOL)cacheResponse;


/**
 *  缓存策略
 *
 *  @return NSURLRequestCachePolicy
 */
- (NSURLRequestCachePolicy)cachePolicy;

/*--------------------------------------------*/
//MARK: ohter config
/**
 *  当数据返回 null 时是否删除这个字段的值，也就是为 nil，默认YES。因为如果数据返回 null 时，Json会把这个null解析成NSNull对象，当向这个对象发送信息时会导致崩溃，所以在这里对返回数据中的 null 值进行处理判断。
 *
 *  @return YES/NO
 */
- (BOOL)removesKeysWithNullValues;

/**
 *  服务端数据接收类型，比如 GCRequestSerializerTypeJSON 用于 post json 数据
 *
 *  @return 服务端数据接收类型
 */
- (GCRequestSerializerType)requestSerializerType;

/**
 *
 */
- (GCResponseSerializerType)responseSerializerType;

/**
 *  自定义超时时间，默认为60
 *
 *  @return 超时时间
 */
- (NSTimeInterval)requestTimeoutInterval;


/**
 muiltpart 数据，用于上传操作
 用法：在 API 接口中实现代理方法
 - (AFConstructingBlock)constructingBodyBlock {
 return ^(id<AFMultipartFormData> formData) {
 for (UIImage *image in _images) {
 NSData *data = UIImageJPEGRepresentation(image, 1.0);
 NSString *name = @"images";
 NSString *formKey = @"images";
 NSString *type = @"image/jpeg";
 [formData appendPartWithFileData:data name:formKey fileName:name mimeType:type];
 }
 };
 }
 
 @return 用于 muiltpart 的数据 block
 */
- (AFConstructingBlock)constructingBodyBlock;

@end

/*--------------------------------------------*/

/**
 请求成功、失败、完成和进行中的委托方法。
 */
@protocol GCRequestDelegate <NSObject>

@optional

- (void)requestSuccess:(GCBaseRequest *)request;
- (void)requestFinished:(GCBaseRequest *)request error:(NSError *)error;
- (void)requestFailed:(GCBaseRequest *)request error:(NSError *)error;
- (void)requestProgress:(NSProgress *)progress;

@end

/*--------------------------------------------*/

/**
 请求将要成功、停止时的委托方法
 */
@protocol GCRequestAccessory <NSObject>

@optional

- (void)requestWillStart:(id)request;
- (void)requestWillStop:(id)request;
- (void)requestDidStop:(id)request;

@end


#pragma mark -

@interface GCBaseRequest : NSObject

@property (nonatomic, strong) NSURLSessionDataTask *sessionDataTask;
/**
 *  HTTP 请求时的参数，被组装到 URL 中
 *  注意 GET 等请求对长度有限制
 */
@property (nonatomic, strong) NSDictionary *requestArgument;

/**
 *  HTTP 报文内容
 *  GET、POST 皆可生效
 *  注意，如果需要上传是文件，可以直接使用 GCAPIRequest 的 constructingBodyBlock
 */
@property (nonatomic, strong) NSData *requestBody;
/**
 *  用于加入 HTTP Header "Content-Type:"
 *  POST 的 body 不为空时，同时需要配置本属性
 *  若为空，self.requestBody 将不会生效
 */
@property (nonatomic, strong) NSString *contentType;

/**
 *  用于 POST 情况下，拼接参数请求，而不是放在 body 里面
 *  如果检查到该参数，Request 会直接在 URL 后拼接 “?key1=value1&key2=value2”。
 **
 *  如果 API 同时拥有 requestArgument 和 queryArgumen 会出现参数拼接错误
 *  可以使用 [GCNetworkConfig sharedInstance].uniReformer
 *  processArgumentWithRequest: query: 方法统一进行处理
 */
@property (nonatomic, strong) NSDictionary<NSString *, NSString *> *queryArgument;
@property (nonatomic, weak) id<GCRequestDelegate> delegate;
@property (nonatomic, weak, readonly) id<GCAPIRequest> child;
/**
 *  将 JSON 建模后的 model 数组
 */
@property (nonatomic, strong) NSArray *modeledResponseObject;

/**
 * 过滤后的 JSON 数据
 */
@property (nonatomic, strong) id filteredJSONResponseObject;
/**
 *  接口返回的原始数据
 */
@property (nonatomic, strong) id rawJSONResponseObject;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong, readonly) id cacheJson;
@property (nonatomic, strong, readonly) NSString *urlString;
@property (nonatomic, assign, readonly) GCRequestStatus status;
@property (nonatomic, copy) void (^successCompletionBlock)(GCBaseRequest *);
@property (nonatomic, copy) void (^failureCompletionBlock)(GCBaseRequest *, NSError *error);
@property (nonatomic, copy) void (^finishedCompletionBlock)(GCBaseRequest *, NSError *error);
@property (nonatomic, copy) void (^progressBlock)(NSProgress * progress);

- (void)start;

- (void)stop;

- (void)startWithBlockSuccess:(GCRequestCompletionBlock)success
                      failure:(GCRequestFailureBlock)failure;


- (void)startWithBlockSuccess:(GCRequestCompletionBlock)success
                      failure:(GCRequestFailureBlock)failure
                     finished:(GCRequestFinishedBlock)finished;


- (void)startWithBlockProgress:(void (^)(NSProgress *progress))progress
                       success:(GCRequestCompletionBlock)success
                       failure:(GCRequestFailureBlock)failure;

- (void)startWithBlockProgress:(void (^)(NSProgress *progress))progress
                       success:(GCRequestCompletionBlock)success
                       failure:(GCRequestFailureBlock)failure
                      finished:(GCRequestFinishedBlock)finished;

- (void)clearCompletionBlock;

@end
