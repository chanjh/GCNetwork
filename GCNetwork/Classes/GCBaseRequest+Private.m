//
//  GCBaseRequest+Private.m
//  GCNetwork
//
//  Created by 陈嘉豪 on 2018/6/27.
//

#import "GCBaseRequest+Private.h"
#import <objc/runtime.h>

@implementation GCBaseRequest(Private)

- (void)changeStatus:(GCRequestStatus)status{
    self.status = status;
}

- (GCRequestStatus)status {
    return [objc_getAssociatedObject(self, _cmd) intValue];
}

- (void)setStatus:(GCRequestStatus)status{
    objc_setAssociatedObject(self, @selector(status), @(status), OBJC_ASSOCIATION_ASSIGN);
}

@end
