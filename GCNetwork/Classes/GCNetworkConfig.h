//
//  GCNetworkConfig.h
//
//  Created by 陈嘉豪 on 2017/9/10.
//  Copyright © 2017年 Gill Chan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCReformerProtocol.h"

@class GCBaseRequest;
@class AFSecurityPolicy;

#pragma mark -

@interface GCNetworkConfig : NSObject

+ (GCNetworkConfig *)sharedInstance;

/**
 请求所用的主 URL
 */
@property (nonatomic, strong) NSString *mainBaseUrl;

/**
 请求所用的副 URL
 */
@property (nonatomic, strong) NSString *viceBaseUrl;
@property (nonatomic, strong) AFSecurityPolicy *securityPolicy;

/**
 * 数据统一过滤器
 */
@property (nonatomic, strong) id <GCReformerProtocol> uniReformer;

// 状态栏显示加载
@property (nonatomic, assign) BOOL networkActivityIndicatorVisible;

@end

