//
//  GCNetworkConstant.h
//  GCNetwork
//
//  Created by 陈嘉豪 on 2018/4/2.
//  Copyright © 2018年 Gill Chan. All rights reserved.
//

# pragma mark - GCNetwork 常量

#ifndef GCNetworkConstant_h
#define GCNetworkConstant_h

/* ---- Notification Key  ---- */

#define kGCNetworkErrorNTFKey @"kGCNetworkErrorNTFKey"

#endif /* GCNetworkConstant_h */
