//
//  GCNetworkConfig.m
//
//  Created by 陈嘉豪 on 2017/9/10.
//  Copyright © 2017年 Gill Chan. All rights reserved.
//

#import "GCNetworkConfig.h"
#import "AFNetworking.h"

@implementation GCNetworkConfig

+ (GCNetworkConfig *)sharedInstance {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        _securityPolicy = [AFSecurityPolicy defaultPolicy];
        _networkActivityIndicatorVisible = YES;
    }
    return self;
}

@end


