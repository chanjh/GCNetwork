//
//  GCNetworkAgent.m
//
//  Created by 陈嘉豪 on 2017/9/10.
//  Copyright © 2017年 Gill Chan. All rights reserved.
//

#import "GCNetworkAgent.h"
#import "GCBaseRequest.h"
#import "GCBaseRequest+Private.h"
#import "GCNetworkConfig.h"
#import "Reachability/Reachability.h"
#import "AFNetworking.h"
#import "YYCache.h"

@interface GCNetworkAgent()

@property (nonatomic, strong) AFHTTPSessionManager *manager;
@property (nonatomic, strong) NSMutableDictionary *requestsRecord;
@property (nonatomic, strong) GCNetworkConfig *config;

@end

@implementation GCNetworkAgent

#pragma mark - life cycle

- (instancetype)init{
    if(self = [super init]){
        self.config = [GCNetworkConfig sharedInstance];
        self.requestsRecord = [NSMutableDictionary dictionary];
        self.manager.securityPolicy = self.config.securityPolicy;
        self.manager = [AFHTTPSessionManager manager];
        self.manager.operationQueue.maxConcurrentOperationCount = 4;
    }
    return self;
}

//+ (instancetype)shareInstance{
//    static dispatch_once_t onceToken;
//    static GCNetworkAgent *agent = nil;
//    dispatch_once(&onceToken, ^{
//        agent = [[GCNetworkAgent alloc]init];
//        agent.config = [GCNetworkConfig sharedInstance];
//        agent.requestsRecord = [NSMutableDictionary dictionary];
//        agent.manager.securityPolicy = agent.config.securityPolicy;
//        agent.manager = [AFHTTPSessionManager manager];
//        agent.manager.operationQueue.maxConcurrentOperationCount = 4;
//    });
//    return agent;
//}

- (void)dealloc{
    [self.manager invalidateSessionCancelingTasks:NO];
}

#pragma mark - request handle

/**
 新建一个请求，并判断该请求需要进行的操作，并针对不同操作对其进行不同的解析操作。
 
 @param request 是由各个 API 发起的请求，可以是 POST 或者是 GET
 */
- (void)addRequest:(GCBaseRequest <GCAPIRequest>*)request {
    
    // 检查网络是否通畅
    if(![self checkNetworkConnection])
    {
        [self sendNetworkNotificationForRequest:request];
        return;
    }
    
    NSString *url = request.urlString;
    
    // 
    if ([request.child respondsToSelector:@selector(responseSerializerType)] &&
        [request.child responseSerializerType] == GCResponseSerializerTypeHTTP) {
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else{
        AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
        serializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain", nil];
        
        // TODO: 检查是否对返回数据中的 null 进行处理
        if ([request.child respondsToSelector:@selector(removesKeysWithNullValues)]) {
            serializer.removesKeysWithNullValues = [request.child removesKeysWithNullValues];
        }
        
        self.manager.responseSerializer = serializer;
    }
    NSDictionary *argument = request.requestArgument;
    
    // 检查是否有统一的参数加工
    if (self.config.uniReformer && [self.config.uniReformer respondsToSelector:@selector(processArgumentWithRequest:query:)])
    {
        argument = [self.config.uniReformer processArgumentWithRequest:request.requestArgument
                                                                 query:request.queryArgument];
    }
    
    // TODO: 检查服务端数据接收类型
    if ([request.child respondsToSelector:@selector(requestSerializerType)]) {
        if ([request.child requestSerializerType] == GCRequestSerializerTypeHTTP) {
            self.manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        }else{
            self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        }
    }
    
    // 检查是否使用自定义超时时间
    if ([request.child respondsToSelector:@selector(requestTimeoutInterval)]) {
        self.manager.requestSerializer.timeoutInterval = [request.child requestTimeoutInterval];
    }
    else{
        self.manager.requestSerializer.timeoutInterval = 60.0;
    }
    
    // TODO: 检查缓存策略
    if ([request.child respondsToSelector:@selector(cachePolicy)]) {
        [self.manager.requestSerializer setCachePolicy:[request.child cachePolicy]];
    }
    else{
        [self.manager.requestSerializer setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    }
    // 状态栏显示加载
    if(self.config.networkActivityIndicatorVisible){
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
    // 请求开始发出
    [request changeStatus:GCRequestStatusConnecting];
    // TODO: 处理 GET 请求
    if ([request.child requestMethod] == GCRequestMethodGet) {
        request.sessionDataTask = [self.manager GET:url parameters:argument progress:^(NSProgress * _Nonnull downloadProgress) {
            [self handleRequestProgress:downloadProgress request:request];
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self handleSuccessBlockWithTask:task request:request andResponse:responseObject];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self handleRequestFailure:task error:error];
        }];
    }
    // TODO: 处理 POST 请求
    else if ([request.child requestMethod] == GCRequestMethodPost){
        // TODO: 处理上传操作
        if ([request.child respondsToSelector:@selector(constructingBodyBlock)] && [request.child constructingBodyBlock]) {
            request.sessionDataTask = [self.manager POST:url parameters:argument constructingBodyWithBlock:[request.child constructingBodyBlock] progress:^(NSProgress * _Nonnull uploadProgress) {
                [self handleRequestProgress:uploadProgress request:request];
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [self handleSuccessBlockWithTask:task request:request andResponse:responseObject];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self handleRequestFailure:task error:error];
            }];
        }
        else if (request.requestBody && request.contentType){
            NSMutableURLRequest *urlRequest = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:argument error:nil];
            urlRequest.timeoutInterval= self.manager.requestSerializer.timeoutInterval;
            [urlRequest setValue:request.contentType forHTTPHeaderField:@"Content-Type"];
            [urlRequest setHTTPBody:request.requestBody];
            
            request.sessionDataTask = [self.manager dataTaskWithRequest:urlRequest uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
                [self handleRequestProgress:uploadProgress request:request];
            } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
                
            } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                if(error){
                    [self handleRequestFailure:request.sessionDataTask error:error];
                }else{
                    [self handleSuccessBlockWithTask:request.sessionDataTask request:request andResponse:responseObject];
                }
            }];
            [request.sessionDataTask resume];
        }
        else
        {
            request.sessionDataTask = [self.manager POST:url parameters:argument progress:^(NSProgress * _Nonnull uploadProgress) {
                [self handleRequestProgress:uploadProgress request:request];
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [self handleSuccessBlockWithTask:task request:request andResponse:responseObject];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self handleRequestFailure:task error:error];
            }];
        }
    }
    [self addOperation:request];
}

/**
 处理 Success Block 内逻辑
 
 @param task 请求任务
 @param request API 中的请求
 @param responseObject 响应体
 */
- (void)handleSuccessBlockWithTask:(NSURLSessionDataTask * _Nonnull)task
                           request:(GCBaseRequest <GCAPIRequest>*)request
                       andResponse:(id  _Nullable)responseObject
{
    // TODO: 处理返回的错误数据
    if(self.config.uniReformer && [self.config.uniReformer respondsToSelector:@selector(validResponseObject:)])
    {
        if([self.config.uniReformer validResponseObject:responseObject])
        {
            request.rawJSONResponseObject = responseObject;
            [self handleRequestSuccess:task];
        }
        else
        {
            NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorBadServerResponse userInfo:nil];
            request.rawJSONResponseObject = responseObject;
            [self handleRequestFailure:task error:error];
        }
    }else{
        request.rawJSONResponseObject = responseObject;
        [self handleRequestSuccess:task];
    }
}

/**
 处理请求进程
 
 @param progress 请求的进程
 @param request API中的请求
 */
- (void)handleRequestProgress:(NSProgress *)progress request:(GCBaseRequest *)request{
    if (request.delegate && [request.delegate respondsToSelector:@selector(requestProgress:)]) {
        [request.delegate requestProgress:progress];
    }
    if (request.progressBlock) {
        request.progressBlock(progress);
    }
}

/**
 请求完成后的处理
 
 @param sessionDataTask NSURLSessionDataTask用来下载数据到内存里，数据的格式是NSData，同时本方法对相关操作进行缓存处理
 */
- (void)handleRequestSuccess:(NSURLSessionDataTask *)sessionDataTask{
    NSString *key = [self keyForRequest:sessionDataTask];
    GCBaseRequest *request = _requestsRecord[key];
    [request changeStatus:GCRequestStatusSuccess];
    if (request) {
        // 状态栏隐藏加载
        if([self isAgentBusy]){        
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
        //更新缓存
        if (([request.child respondsToSelector:@selector(cacheResponse)] && [request.child cacheResponse])) {
            YYCache *yyCache = [YYCache cacheWithName:@"cacheJson"];
            NSMutableString *key = [NSMutableString stringWithString:request.urlString];
            if([request.requestArgument allValues].count){
                [key appendFormat:@"?"];
                for(NSString *dicKey in request.requestArgument){
                    [key appendFormat:@"%@=%@&",dicKey,request.requestArgument[dicKey]];
                }
                [key deleteCharactersInRange:NSMakeRange(key.length - 1, 1)];
            }
            [yyCache.diskCache setObject:request.filteredJSONResponseObject forKey:[key copy]];
        }
        
        if (request.delegate != nil && [request.delegate respondsToSelector:@selector(requestSuccess:)]) {
            [request.delegate requestSuccess:request];
        }
        if (request.delegate != nil && [request.delegate respondsToSelector:@selector(requestFinished:error:)]) {
            [request.delegate requestFinished:request error:nil];
        }
        
        if (request.successCompletionBlock) {
            request.successCompletionBlock(request);
        }
        if (request.finishedCompletionBlock) {
            request.finishedCompletionBlock(request, nil);
        }
    }
    
    [self removeOperation:sessionDataTask];
    [request clearCompletionBlock];
}


/**
 请求失败后的处理
 
 @param sessionDataTask NSURLSessionDataTask用来下载数据到内存里，数据的格式是NSData
 @param error 导致请求失败的错误
 */
- (void)handleRequestFailure:(NSURLSessionDataTask *)sessionDataTask error:(NSError *)error{
    NSString *key = [self keyForRequest:sessionDataTask];
    GCBaseRequest *request = _requestsRecord[key];
    request.error = error;
    [request changeStatus:GCRequestStatusFailure];
    if (request) {
        // 状态栏隐藏加载
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (request.delegate != nil && [request.delegate respondsToSelector:@selector(requestFailed:error:)]) {
            [request.delegate requestFailed:request error:error];
        }
        if (request.delegate != nil && [request.delegate respondsToSelector:@selector(requestFinished:error:)]) {
            [request.delegate requestFinished:request error:error];
        }
        if (request.failureCompletionBlock) {
            request.failureCompletionBlock(request, error);
        }
        if (request.finishedCompletionBlock) {
            request.finishedCompletionBlock(request, error);
        }
    }
    [self removeOperation:sessionDataTask];
    [request clearCompletionBlock];
}

/**
 取消请求的操作
 
 @param request 需要被取消的请求
 */
- (void)cancelRequest:(GCBaseRequest *)request {
    [request.sessionDataTask cancel];
    [self removeOperation:request.sessionDataTask];
    [request clearCompletionBlock];
}


/**
 移除DataTask的操作
 
 @param operation 需要被移除的操作
 */
- (void)removeOperation:(NSURLSessionDataTask *)operation {
    NSString *key = [self keyForRequest:operation];
    @synchronized(self) {
        [_requestsRecord removeObjectForKey:key];
    }
    if([self.requestsRecord allValues].count == 0){
        [[NSNotificationCenter defaultCenter] postNotificationName:k_GCNetworkAgentNotiNameForUnconnecting object:nil];
    }
}


/**
 增加一个操作请求
 
 @param request 需要被增加的请求
 */
- (void)addOperation:(GCBaseRequest *)request {
    if (request.sessionDataTask != nil) {
        NSString *key = [self keyForRequest:request.sessionDataTask];
        @synchronized(self) {
            self.requestsRecord[key] = request;
        }
    }
    if([self.requestsRecord allValues].count){
        [[NSNotificationCenter defaultCenter] postNotificationName:k_GCNetworkAgentNotiNameForConnecting object:nil];
    }
}


/**
 获取一个请求的健值
 
 @param object 需要获取键值的DataTask
 @return 对应的键值
 */
- (NSString *)keyForRequest:(NSURLSessionDataTask *)object {
    NSString *key = [@(object.taskIdentifier) stringValue];
    return key;
}

#pragma mark - network handle
/**
 检查当前网络环境
 
 @return 当前网络是否联通
 */
- (BOOL)checkNetworkConnection
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable;
}

/**
 @param request 当前的请求
 */
- (void)sendNetworkNotificationForRequest:(GCBaseRequest*)request
{
    NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
    if(request.failureCompletionBlock){    
        request.failureCompletionBlock(request, error);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kGCNetworkErrorNTFKey
                                                        object:request];
}

# pragma mark - Status
- (BOOL)isAgentBusy{
    NSArray <GCBaseRequest*> *requests = [self.requestsRecord allValues];
    for(GCBaseRequest *request in requests){
        if(request.status == GCRequestStatusConnecting){
            return YES;
        }
    }
    return NO;
}

//+ (BOOL)isAgentBusy{
//    GCNetworkAgent *sharedAgent = [GCNetworkAgent shareInstance];
//    return [sharedAgent isAgentBusy];
//}

@end
