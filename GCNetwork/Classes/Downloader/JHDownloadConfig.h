//
//  JHDownloadConfig.h
//  GCNetworkDemo
//
//  Created by 陈嘉豪 on 2018/6/15.
//  Copyright © 2018年 Gill Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JHDownloadConfig : NSObject

+ (instancetype)globalConfig;

// 只在 Wi-Fi 环境下载，默认为 NO
@property (nonatomic, assign) BOOL onlyWifi;
// 连上 Wi-Fi 后自动开始任务，只有 onlyWifi == YES 有效，默认为 YES
@property (nonatomic, assign) BOOL startWhenWifi;

@end
