//
//  JHDownloadManager.m
//  MoSplash
//
//  Created by 陈嘉豪 on 2017/5/25.
//  Copyright © 2017年 陈嘉豪. All rights reserved.
//

#import "JHDownloadManager.h"
#import "Reachability.h"
#import "JHDownloadConfig.h"

@interface JHDownloadManager () <JHDownloadDelegate>
@property (nonatomic, assign) NSUInteger taskCount;
@property (nonatomic, strong) NSMutableArray <JHDownloader *> *mutableTaskArray;
@property (nonatomic, strong) NSRecursiveLock *lock;
@end

@implementation JHDownloadManager

static JHDownloadManager *manager;
+ (instancetype)shareManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[JHDownloadManager alloc]init];
        manager.mutableTaskArray = [NSMutableArray array];
        [[NSNotificationCenter defaultCenter] addObserver:manager
                                                 selector:@selector(reachabilityChanged:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
    });
    return manager;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

# pragma mark - 新建下载线程
+ (void)addTaskWithURL:(NSURL *)url
          withDelegate:(id <JHDownloadManagerDelegate>)target{
    return [self addTaskWithRequest:[NSURLRequest requestWithURL:url]
                       withDelegate:target];
}

+ (void)addTaskWithRequest:(NSURLRequest *)request
              withDelegate:(id <JHDownloadManagerDelegate>)target{
    JHDownloadManager *manager = [JHDownloadManager shareManager];
    // 检查队列中是否存在该任务
    __block BOOL isExist = NO;
    __block NSUInteger index;
    [manager.taskArray enumerateObjectsUsingBlock:^(JHDownloader * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if([obj.downloadID isEqualToString:request.URL.absoluteString])
        {
            isExist = YES;
            index = idx;
        }
    }];
    if(isExist)
    {
        [JHDownloadManager continueTaskWithIndex:index];
        return;
    }
    manager.delegate = target;
    // 创建任务
    [manager addTaskWithRequest:request];
}

- (void)addTaskWithRequest:(NSURLRequest *)request
{
    JHDownloader *downloader = [JHDownloader downloadWithRequest:request delegate:self];
    [self.lock lock];
    [self.mutableTaskArray addObject:downloader];
    [self.lock unlock];
}
# pragma mark - 对存在的下载线程进行操作
// ---------- 非接口方法
+ (void)continueTaskWithIndex:(NSUInteger)index{
    JHDownloader *downloader = [JHDownloadManager shareManager].taskArray[index];
    [downloader continueDownload];
}

+ (void)stopTaskWithIndex:(NSUInteger)index{
    JHDownloader *downloader = [JHDownloadManager shareManager].taskArray[index];
    [downloader stopDownload];
}

// ---------- 接口方法
+ (void)stopAllTask{
    JHDownloadManager *manager = [JHDownloadManager shareManager];
    [manager.taskArray enumerateObjectsUsingBlock:^(JHDownloader * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj stopDownload];
    }];
}
+ (void)continueAllTask{
    JHDownloadManager *manager = [JHDownloadManager shareManager];
    [manager.taskArray enumerateObjectsUsingBlock:^(JHDownloader * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj continueDownload];
    }];
}
+ (void)stopTask:(JHDownloader *)downloader{
    [downloader stopDownload];
}
+ (void)continueTask:(JHDownloader *)downloader{
    [downloader continueDownload];
}

+ (void)stopTaskWithDownloaderID:(NSString *)downloaderID{
    JHDownloadManager *manager = [JHDownloadManager shareManager];
    [manager.taskArray enumerateObjectsUsingBlock:^(JHDownloader * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if([obj.downloadID isEqualToString:downloaderID])
        {
            [obj stopDownload];
        }
    }];
}
+ (void)continueTaskWithDownloaderID:(NSString *)downloaderID{
    JHDownloadManager *manager = [JHDownloadManager shareManager];
    [manager.taskArray enumerateObjectsUsingBlock:^(JHDownloader * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if([obj.downloadID isEqualToString:downloaderID])
        {
            [obj continueDownload];
        }
    }];
}

# pragma mark - 获取下载状态
+ (JHDownloader *)fetchDownloaderWithID:(NSString *)downloaderID
                           andDelegate:(id <JHDownloadManagerDelegate>)target{
    JHDownloadManager *manager = [JHDownloadManager shareManager];
    manager.delegate = target;
    JHDownloader *downloader;
    for (NSInteger index = 0; index<manager.taskArray.count; index++){
        JHDownloader *downloaderInArray = (JHDownloader *)[manager.taskArray objectAtIndex:index];
        if([downloaderInArray.downloadID isEqualToString:downloaderID])
        {
            downloader = downloaderInArray;
            downloader.downloadDelegate = manager;
        }
    }
    return downloader;
}

# pragma mark - 网络状况切换

- (void)reachabilityChanged:(NSNotification* )note
{
    Reachability *curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    NetworkStatus curStatus = [curReach currentReachabilityStatus];
    switch (curStatus) {
        case ReachableViaWWAN:
            [self stopOnlyWifiTask];
            break;
        case ReachableViaWiFi:
            [self continueOnlyWifiTask];
            break;
        default:
            break;
    }
}

- (void)stopOnlyWifiTask{
    for (JHDownloader *downloader in self.taskArray){
        if(downloader.config.onlyWifi && downloader.isDownloading){
            [downloader stopDownload];
        }
    }
}

- (void)continueOnlyWifiTask{
    for (JHDownloader *downloader in self.taskArray){
        if(downloader.config.onlyWifi && downloader.config.startWhenWifi && !downloader.isDownloading){
            [downloader stopDownload];
        }
    }
}

# pragma mark - DownloaderDelegate
/**
 * 下载完成
 */
- (void)finishDownloadWithResponseData:(NSData *)responseData andDownloader:(JHDownloader *)downloader
{
    // 此时下载任务已经完成
    // 将任务从队列中删除
    @synchronized (self) {
        [self.mutableTaskArray enumerateObjectsUsingBlock:^(JHDownloader * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if([obj.downloadID isEqualToString:downloader.downloadID])
            {
                [self.mutableTaskArray removeObject:obj];
            }
        }];
    }
    // 回调
    if([self.delegate respondsToSelector:@selector(taskFinishDownload:withData:)])
    {
        [self.delegate taskFinishDownload:downloader withData:responseData];
    }
}

/**
 * 下载进程
 */
-(void)downloadingWithProgress:(float)progress andDownloader:(JHDownloader *)downloader
{
    if([self.delegate respondsToSelector:@selector(taskDownloadingWithProgress:andDownloader:)])
    {
        [self.delegate taskDownloadingWithProgress:progress andDownloader:downloader];
    }
}

# pragma mark - Getter and setter
- (NSUInteger)taskCount{
    return self.taskArray.count;
}

- (NSArray<JHDownloader *> *)taskArray{
    return [_mutableTaskArray copy];
}

- (NSRecursiveLock *)lock{
    if(!_lock){
        _lock = [[NSRecursiveLock alloc]init];
    }
    return _lock;
}

@end
