//
//  JHDownloader.m
//  MoSplash
//
//  Created by 陈嘉豪 on 2017/5/29.
//  Copyright © 2017年 陈嘉豪. All rights reserved.
//

#import "JHDownloader.h"
#import "JHDownloadManager.h"
#import "JHDownloadConfig.h"
#import "GCNetworkAgent.h"
#define kJHBackgroundDownload @"com.chanjh.JHDownloader.BackgroundID"

@interface JHDownloader()<NSURLSessionDelegate>
@property (nonatomic, assign) BOOL isDownloading;   // 是否正在下载
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic, strong) NSURLSession *backgroundURLSession;
@end

@implementation JHDownloader

# pragma mark - init
- (instancetype)initWithRequest:(NSURLRequest *)request{
    self = [self init];
    _config = [JHDownloadConfig globalConfig];
    _downloadID = request.URL.absoluteString;
    _downloadTask = [self.backgroundURLSession downloadTaskWithRequest:request];
    _isDownloading = NO;
    _onlyFreeTime = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleApiRequestIsBusy)
                                                 name:k_GCNetworkAgentNotiNameForConnecting
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleApiRequestIsFree)
                                                 name:k_GCNetworkAgentNotiNameForConnecting
                                               object:nil];
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

+ (instancetype)downloadWithRequest:(NSURLRequest *)request
                        andDelegate:(id <JHDownloadDelegate>)target{
    return [self downloadWithRequest:request delegate:target];
}

+ (instancetype)downloadWithURL:(NSURL *)url
                       delegate:(id <JHDownloadDelegate>)target{
    return [self downloadWithRequest:[NSURLRequest requestWithURL:url]
                            delegate:target];
}

+ (instancetype)downloadWithURL:(NSURL *)url
                       progress:(JHDownloaderProgressBlock)progressBlock
                        success:(JHDownloaderSuccessBlock)successBlock
                        failure:(JHDownloaderFailureBlock)failureBlock{
    return [self downloadWithRequest:[NSURLRequest requestWithURL:url]
                            progress:progressBlock
                             success:successBlock
                             failure:failureBlock];
}

+ (instancetype)downloadWithRequest:(NSURLRequest *)request
                           delegate:(id <JHDownloadDelegate>)target{
    JHDownloadManager *manager = [JHDownloadManager shareManager];
    __block BOOL isExist = NO;
    __block NSUInteger index;
    [manager.taskArray enumerateObjectsUsingBlock:^(JHDownloader * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if([obj.downloadID isEqualToString:request.URL.absoluteString])
        {
            isExist = YES;
            index = idx;
        }
    }];
    JHDownloader *downloader;
    if(!isExist){
        JHDownloader *downloader = [[JHDownloader alloc]initWithRequest:request];
        downloader.downloadDelegate = target;
    }else{
        downloader = manager.taskArray[index];
    }
    if(!downloader.onlyFreeTime){
        [downloader continueDownload];
    }
    return downloader;
}

+ (instancetype)downloadWithRequest:(NSURLRequest *)request
                           progress:(JHDownloaderProgressBlock)progressBlock
                            success:(JHDownloaderSuccessBlock)successBlock
                            failure:(JHDownloaderFailureBlock)failureBlock
{
    JHDownloadManager *manager = [JHDownloadManager shareManager];
    __block BOOL isExist = NO;
    __block NSUInteger index;
    [manager.taskArray enumerateObjectsUsingBlock:^(JHDownloader * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if([obj.downloadID isEqualToString:request.URL.absoluteString])
        {
            isExist = YES;
            index = idx;
        }
    }];
    JHDownloader *downloader;
    if(!isExist){
        downloader = [[JHDownloader alloc]initWithRequest:request];
        downloader.progressBlock = progressBlock;
        downloader.successBlock = successBlock;
        downloader.failureBlock = failureBlock;
    }else{
        downloader = manager.taskArray[index];
    }
    if(!downloader.onlyFreeTime){
        [downloader continueDownload];
    }
    return downloader;
}

# pragma mark -

- (void)continueDownload{
    if(self.downloadTask){
        [self.downloadTask resume];
        self.isDownloading = YES;
    }
}

- (void)stopDownload{
    if(self.downloadTask){
        [self.downloadTask suspend];
        self.isDownloading = NO;
    }
}

# pragma mark - Notification
- (void)handleApiRequestIsBusy{
    if(self.onlyFreeTime && self.isDownloading){
        [self stopDownload];
    }
}

- (void)handleApiRequestIsFree{
    if(self.onlyFreeTime && self.isDownloading){
        [self continueDownload];
    }
}

# pragma mark - Delegate
/*
 当一个下载task任务完成以后，这个方法会被调用。我们可以在这里移动或者复制download的数据
 */
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location{
    NSData *data = [NSData dataWithContentsOfURL:location];
    // 传递已经下载的数据用于保存
    if([self.downloadDelegate respondsToSelector:@selector(finishDownloadWithResponseData: andDownloader:)])
    {
        [self.downloadDelegate finishDownloadWithResponseData:data andDownloader:self];
    }
    if(self.successBlock){
        self.successBlock(self, data);
    }
    self.isDownloading = NO;
}

/*
 获取下载进度
 */
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    if([self.downloadDelegate respondsToSelector:@selector(downloadingWithProgress:andDownloader:)])
    {
        [self.downloadDelegate downloadingWithProgress:((float)totalBytesWritten/(float)totalBytesExpectedToWrite) andDownloader:self];
    }
    if(self.progressBlock){
        self.progressBlock(self, (float)totalBytesWritten/(float)totalBytesExpectedToWrite);
    }
}

/**
 * 下载失败
 */
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(nullable NSError *)error
{
    if([self.downloadDelegate respondsToSelector:@selector(finishWithError:)])
    {
        [self.downloadDelegate finishWithError:error];
    }
    if(self.failureBlock){
        self.failureBlock(self, error);
    }
}

/*
 重启一个下载任务(比如下载一半后停止然后过一点时间继续)。如果下载出错，`NSURLSessionDownloadTaskResumeData`里面包含重新开始下载的数据。
 */
- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes{
    // TODO
    NSLog(@"好啦，重新下载啦");
}

#pragma mark - Getter && Setter
- (NSURLSession *)backgroundURLSession {
    if(!_backgroundURLSession){
        NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:[NSString stringWithFormat:@"%@%@",_downloadID,kJHBackgroundDownload]];
        _backgroundURLSession = [NSURLSession sessionWithConfiguration:sessionConfig
                                                delegate:self
                                           delegateQueue:[NSOperationQueue mainQueue]];
    }
    return _backgroundURLSession;
}
@end
