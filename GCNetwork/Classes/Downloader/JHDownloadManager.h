//
//  JHDownloadManager.h
//  MoSplash
//
//  Created by 陈嘉豪 on 2017/5/25.
//  Copyright © 2017年 陈嘉豪. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JHDownloader.h"

@protocol JHDownloadManagerDelegate <NSObject>

@optional

- (void)taskDownloadingWithProgress:(float)progress andDownloader:(JHDownloader *)downloader;
- (void)taskFinishDownload:(JHDownloader *)downloader withData:(NSData *)data;

@end

@interface JHDownloadManager : NSObject

@property (nonatomic, assign, readonly) NSUInteger taskCount;
@property (nonatomic, strong, readonly) NSArray <JHDownloader *> *taskArray;
@property (nonatomic, weak) id<JHDownloadManagerDelegate> delegate;

+ (instancetype)shareManager;

/**
 * 新建下载操作
 */
+ (void)addTaskWithRequest:(NSURLRequest *)request withDelegate:(id <JHDownloadManagerDelegate>)target;
+ (void)addTaskWithURL:(NSURL *)url withDelegate:(id <JHDownloadManagerDelegate>)target;

/**
 * 对下载任务操作
 */
+ (void)stopAllTask;
+ (void)continueAllTask;
+ (void)stopTask:(JHDownloader *)downloader;
+ (void)continueTask:(JHDownloader *)downloader;
+ (void)stopTaskWithDownloaderID:(NSString *)downloaderID;
+ (void)continueTaskWithDownloaderID:(NSString *)downloaderID;

/**
 * 获取下载状态
 */
+ (JHDownloader *)fetchDownloaderWithID:(NSString *)downloaderID
                           andDelegate:(id <JHDownloadManagerDelegate>)target;


@end
