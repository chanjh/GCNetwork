//
//  JHDownloadConfig.m
//  GCNetworkDemo
//
//  Created by 陈嘉豪 on 2018/6/15.
//  Copyright © 2018年 Gill Chan. All rights reserved.
//

#import "JHDownloadConfig.h"

@implementation JHDownloadConfig

+ (instancetype)globalConfig{
    static dispatch_once_t onceToken;
    static JHDownloadConfig *config = nil;
    dispatch_once(&onceToken, ^{
        config = [[JHDownloadConfig alloc]init];
    });
    return config;
}

- (instancetype)init{
    self = [super init];
    self.onlyWifi = NO;
    self.startWhenWifi = YES;
    return self;
}

@end
