//
//  JHDownloader.h
//  MoSplash
//
//  Created by 陈嘉豪 on 2017/5/29.
//  Copyright © 2017年 陈嘉豪. All rights reserved.
//

#import <Foundation/Foundation.h>
@class JHDownloader;
@class JHDownloadConfig;

@protocol JHDownloadDelegate <NSObject>
@optional
-(void)finishDownloadWithResponseData:(NSData *)responseData andDownloader:(JHDownloader *)downloader;

-(void)downloadingWithProgress:(float)progress andDownloader:(JHDownloader *)downloader;

-(void)finishWithError:(NSError *)error;

@end

typedef void (^JHDownloaderProgressBlock)(JHDownloader *downloader, float progress);
typedef void (^JHDownloaderSuccessBlock)(JHDownloader *downloader, NSData *responseData);
typedef void (^JHDownloaderFailureBlock)(JHDownloader *downloader, NSError *error);

@interface JHDownloader : NSObject

/**
 * 回调 Block
 */
@property (readwrite, nonatomic, copy) JHDownloaderProgressBlock progressBlock;
@property (readwrite, nonatomic, copy) JHDownloaderSuccessBlock successBlock;
@property (readwrite, nonatomic, copy) JHDownloaderFailureBlock failureBlock;

@property (nonatomic, weak) id<JHDownloadDelegate> downloadDelegate;
@property (nonatomic, readonly) BOOL isDownloading;   // 是否正在下载
@property (nonatomic, strong) NSString *downloadID; // 唯一标识符，默认是 URL 的字符串
@property (nonatomic, strong) JHDownloadConfig *config;
// 只在非 API 时间下载，默认为 NO
// API 时间: [GCNetworkAgent isBusy]
@property (nonatomic, assign) BOOL onlyFreeTime;

# pragma mark - Init
+ (instancetype)downloadWithRequest:(NSURLRequest *)request
                        andDelegate:(id <JHDownloadDelegate>)target  DEPRECATED_MSG_ATTRIBUTE("Please use [JHDownloader downloadWithRequest: delegate:]");

+ (instancetype)downloadWithURL:(NSURL *)url
                       delegate:(id <JHDownloadDelegate>)target;

+ (instancetype)downloadWithURL:(NSURL *)url
                       progress:(JHDownloaderProgressBlock)progressBlock
                        success:(JHDownloaderSuccessBlock)successBlock
                        failure:(JHDownloaderFailureBlock)failureBlock;

+ (instancetype)downloadWithRequest:(NSURLRequest *)request
                           delegate:(id <JHDownloadDelegate>)target;

+ (instancetype)downloadWithRequest:(NSURLRequest *)request
                           progress:(JHDownloaderProgressBlock)progressBlock
                            success:(JHDownloaderSuccessBlock)successBlock
                            failure:(JHDownloaderFailureBlock)failureBlock;

/**
 * 下载操作方法
 */
- (void)continueDownload;
- (void)stopDownload;

@end
